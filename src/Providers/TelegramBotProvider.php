<?php

namespace Mfissehaye\TelegramBot\Providers;

use Illuminate\Support\ServiceProvider;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\TelegramLog;

class TelegramBotProvider extends ServiceProvider
{
    /**
     * Indicates that loading of the provider is deferred
     * @var bool
     */
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/telegram.php', 'telegram'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('telegram', function ($app) {

            try {
                $telegram = new Telegram(config('telegram.bots.common.token'), config('telegram.bots.common.username'));
                $telegram->handle();
            } catch (TelegramException $exception) {
                TelegramLog::error($exception->getMessage());
            }
        });
    }

    public function provides()
    {
        return [Telegram::class];
    }
}
